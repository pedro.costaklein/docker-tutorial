# docker-tutorial

## Description

This repository contains the example application to be ported to a Docker container for the CRC Docker hands-on tutorial.

Example was extracted from the Optimal Transport Livedoc (https://gitlab.gwdg.de/crc1456/livedocs/guided-jupyter-tour-optimal-transport)

